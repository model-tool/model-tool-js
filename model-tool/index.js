const ModelProcessor = require('./ModelProcessor.js');
const DataProcessor = require('./DataProcessor.js');

module.exports = function main(args) {
  console.log('Start...');

  let workingDirPath = '../';
  let baseModelDirPath = './model';
  let baseModelFileName = 'Base-Relational-Model.xlsx';
  let baseDataFileName = 'Base-Data.xlsx';
  let modelDirPath = '../test/doc/model';
  let modelFileName = 'Relational-Model.xlsx';
  let dataFileName = 'Data.xlsx';
  let sourceDirPath = '../test/src/';
  let modelSqlFileName = 'Generated-Create-Tables.sql';
  let dataSqlFileName = 'Generated-Insert-Rows.sql';

  let modelProcessor = new ModelProcessor(
    workingDirPath, baseModelDirPath, modelDirPath, sourceDirPath,
    baseModelFileName, modelFileName, modelSqlFileName);
  modelProcessor.process();

  let dataProcessor = new DataProcessor(
    modelProcessor, baseDataFileName, dataFileName, dataSqlFileName);
  dataProcessor.process();

  console.log('End.');
}
