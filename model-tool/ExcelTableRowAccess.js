const UTIL = require('./util');

module.exports = class ExcelTableRowAccess {

  /**
   * The excel sheet that contains the underlying table.
   */
  get sheet() { return this._sheet; }

  /**
   * The names of the columns in the underlying table.
   */
  get columnNames() { return this._colNames; }

  /**
   * The language codes paired to the column names by index.
   */
  get languageCodes() { return this._langCodes; }

  /**
   * All codes of languages that were used to provide localized column values.
   */
  get usedLanguageCodes() { return this._usedLangCodes; }

  /**
   * The count of columns in the underlying table.
   */
  get columnCount() { return this._colNames.length; }

  /**
   * The current row in the underlying table.
   */
  get row() { return this._row; }

  /**
   * The index of the current row in the underlying table.
   */
  get rowIndex() { return this._rx; }

  /**
   * Does the underlying table contain a localized column?
   */
  get isLocalized() { return this._isLocalized; }

  constructor(sheet, rowIndex = 0) {
    this._sheet = sheet;
    this._row = [];
    this._initRx = rowIndex;
    this._colNames = [];
    this._langCodes = [];
    this._usedLangCodes = [];
    this._isLocalized = false;
    this._rx = rowIndex;
    this.reset();
  }

  reset() {
    this._colNames = [];
    this._langCodes = [];
    this._usedLangCodes = [];
    this._isLocalized = false;
    this._rx = this._initRx;
    for (let cx = 0; ; cx++) {
      let cellValue = ExcelTableRowAccess.getCell(this._sheet, this._rx, cx);
      if (UTIL.isNullOrWhiteSpace(cellValue)) {
        break;
      }
      let langCode = null;
      if (cellValue.indexOf('(') == -1) {
        this._colNames.push(cellValue);
      }
      else {
        this._colNames.push(cellValue.substring(0, cellValue.indexOf('(') - 1));
        this._isLocalized = true;
        langCode = cellValue.substr(cellValue.indexOf('(') + 1, 2)
      }
      this._langCodes.push(langCode);
      if (langCode != null && this._usedLangCodes.indexOf(langCode) == -1) {
        this._usedLangCodes.push(langCode);
      }
    }
  }

  *[Symbol.iterator]() {
    for (this._rx++; ; this._rx++) {
      this._row = [];
      let cellValue = ExcelTableRowAccess.getCell(this._sheet, this._rx, 0);
      if (UTIL.isNullOrWhiteSpace(cellValue)) {
        break;
      }
      this._row.push(cellValue);
      for (let cx = 1; cx < this._colNames.length; cx++) {
        cellValue = ExcelTableRowAccess.getCell(this._sheet, this._rx, cx);
        this._row.push(cellValue);
      }
      yield this;
    }
  }

  static getCell(sheet, rx, cx) {
    try {
      let address = String.fromCharCode(65 + cx) + (1 + rx).toString()
      let cell = sheet[address];
      return (cell ? cell.v : undefined);
    }
    catch (e) {
      // The caller must take care of the table dimensions and interpreting empty
      // values.
    }
    return null;
  }

  getString(cx) {
    return this._row[cx];
  }

  getStringFlattened(cx) {
    let text = this._row[cx];
    if (!UTIL.isNullOrWhiteSpace(text)) {
      text = text.replace(',\r\n', ', ');
      text = text.replace(',\r', ', ');
      text = text.replace(',\n', ', ');
      text = text.replace('\r\n', '');
      text = text.replace('\r', '');
      text = text.replace('\n', '');
    }
    return text;
  }

  getBoolean(cx) {
    let value = this._row[cx];
    return (value == 'Y' || value == 'y') ? true : false;
  }

  getInteger(cx) {
    let value = this._row[cx];
    if (value != null) {
      return parseInt(value);
    }
    return 0;
  }

}
