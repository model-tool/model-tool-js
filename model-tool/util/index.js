const FS = require('fs');
const PATH = require('path');

function coalesce() {
  let value = arguments[0];
  for (let ax = 1; ax < arguments.length; ax++) {
    if (!isNull(value)) {
      break;
    }
    value = arguments[ax];
  }
  return value;
}

function isNull(value) {
  //~ console.log(typeof value);
  return (typeof(value) == 'undefined' ||
          value === null);
}

function isNullOrEmpty(value) {
  if (typeof value == 'undefined' ||
      value === null)
  {
    return true;
  }
  if (value instanceof String) {
    return value.length == 0;
  }
  return false;
}

function isNullOrWhiteSpace(value) {
  if (typeof value == 'undefined' ||
      value === null)
  {
    return true;
  }
  if (value instanceof String) {
    return value.trim().length == 0;
  }
  return false;
}

function defaulted(value, default_value) {
  return isNull(value) ? default_value : value;
}

function rightPad(str, padLen, padChar) {
  padChar = defaulted(padChar, ' ').substr(0, 1);
  var res = str;
  for (i = 0; i < padLen - str.length; i++) {res += padChar;}
  return res;
}

function leftPad(str, padLen, padChar) {
  padChar = defaulted(padChar, ' ').substr(0, 1);
  var res = '';
  for (i = 0; i < padLen - str.length; i++) {res += padChar;}
  res += str;
  return res;
}

function capitalize(value) {
  if (!isNullOrWhiteSpace(value)) {
    // console.log('typeof value: ' + typeof value);
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
  return null;
}

function uncapitalize(value) {
  if (!isNullOrWhiteSpace(value)) {
    // console.log('typeof value: ' + typeof value);
    return value.charAt(0).toLowerCase() + value.slice(1);
  }
  return null;
}

function ensureDirsExists(path) {
  dirpath = PATH.dirname(path);
  if (dirpath != path) {
    ensureDirsExists(dirpath);
    if (!FS.existsSync(path)) {
      FS.mkdirSync(path);
    }
  }
}

function createWriteStream(path, options) {
  ensureDirsExists(PATH.dirname(path));
  return FS.createWriteStream(path, options);
}

module.exports = {
  coalesce: coalesce,
  isNull: isNull,
  isNullOrEmpty: isNullOrEmpty,
  isNullOrWhiteSpace: isNullOrWhiteSpace,
  rightPad: rightPad,
  leftPad: leftPad,
  capitalize: capitalize,
  uncapitalize: uncapitalize,
  ensureDirsExists: ensureDirsExists,
  createWriteStream: createWriteStream,
};
