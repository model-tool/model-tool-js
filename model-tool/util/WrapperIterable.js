module.exports = class WrapperIterable {

  constructor(obj, generator) {
    this._obj = obj;
    this._generator = generator;
    this._generator = this._generator.call(this._obj);
  }

  [Symbol.iterator]() { return this._generator; }
  values() { return this._generator; }

  *keys() {
    let generator = this._generator.call(this._obj);
    let result = generator.next();
    let index = 0;
    while (!result.done) {
      yield index++;
      result = generator.next();
    }
  }

  *entries() {
    let generator = this._generator.call(this._obj);
    let result = generator.next();
    let index = 0;
    while (!result.done) {
      yield [index++, result.value];
      result = generator.next();
    }
  }

}
