let Enum = require('./Enum.js');

module.exports = class Flags extends Enum {

  constructor(name, id) { super(name, id); }

  hasFlag(value) { return (this._id & value._id) == value._id; }

  add(value) {
    let id = this._id | value._id;
    if (this.constructor[id] != undefined) {
      return this.constructor[id];
    }
    return Object.freeze(new this.constructor('', id));
  }

  remove(value) {
    let id = this._id - (this._id & value._id);
    if (this.constructor[id] != undefined) {
      return this.constructor[id];
    }
    return Object.freeze(new this.constructor('', id));
    // return Object.freeze(new this.constructor(`${this._name} | ${value._name}`, id));
  }

  static define() {
    // The arguments are names, the have to be normalized.
    // These names will become the keys to get the enum values.
    let names = [];
    for (var i = 0; i < arguments.length; i++) {
      names.push(arguments[i].
        replace('\s', '_').
        replace('-', '_').
        replace(/_{2,}/g, '_').
        toUpperCase());
    }

    let values = [];
    // var enumType = function (name, id) { Flags.call(this, name, id); };
    // enumType.prototype = Object.create(Flags.prototype);
    // enumType.prototype.constructor = enumType;
    let enumType = class DerivedFlags extends Flags {
      static get values() { return values; }
      constructor(name, id) { super(name, id); }
    }

    let id = 1;
    for (let name of names) {
      let value = Object.freeze(new enumType(name, id));
      values.push(value);
      enumType[name] = value;
      enumType[id] = value;
      id *= 2;
    }
    return Object.freeze(enumType);
  }

}
