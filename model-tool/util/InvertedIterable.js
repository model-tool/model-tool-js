const UTIL = require('../util');

module.exports = class InvertedIterable {

  constructor(array, predicate, successor) {
    this._array = array;
    this._predicate = UTIL.coalesce(predicate, (element) => true);
    this._successor = UTIL.coalesce(successor, (element) => null);
  }

  *[Symbol.iterator]() {
    for (let i = this._array.length; i >= 0; i--) {
      yield this._array[i];
    }
  }

}
