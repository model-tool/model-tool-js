module.exports = class Enum {

  static get values() { return null; }
  get name() { return this._name; }
  get id() { return this._id; }

  constructor(name, id) {
    this._name = name;
    this._id = id;
  }

  valueOf() { return this._id; }

  static define() {
    // The arguments are names, the have to be normalized.
    // These names will become the keys to get the enum values.
    let names = [];
    for (var i = 0; i < arguments.length; i++) {
      names.push(arguments[i].
        replace('\s', '_').
        replace('-', '_').
        replace(/_{2,}/g, '_').
        toUpperCase());
    }

    let values = [];
    let enumType = class DerivedEnum extends Enum {
      static get values() { return values; }
      constructor(name, id) { super(name, id); }
    }

    let id = 1;
    for (let name of names) {
      let value = Object.freeze(new enumType(name, id));
      values.push(value);
      enumType[name] = value;
      enumType[id] = value;
      id++;
    }
    return Object.freeze(enumType);
  }

}
