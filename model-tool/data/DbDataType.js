const Enum = require('../util/Enum.js');

const DbDataType = Enum.define(
    /// <summary>
    /// Universally Unique Identifier
    /// </summary>
    'UUID',
    /// <summary>
    /// Signed Integer, 8 bits
    /// </summary>
    'I8',
    /// <summary>
    /// Signed Integer, 16 bits
    /// </summary>
    'I16',
    /// <summary>
    /// Signed Integer, 32 bits
    /// </summary>
    'I32',
    /// <summary>
    /// Signed Integer, 64 bits
    /// </summary>
    'I64',
    /// <summary>
    /// Unsigned Integer, 8 bits
    /// </summary>
    'UI8',
    /// <summary>
    /// Unsigned Integer, 16 bits
    /// </summary>
    'UI16',
    /// <summary>
    /// Unsigned Integer, 32 bits
    /// </summary>
    'UI32',
    /// <summary>
    /// Unsigned Integer, 64 bits
    /// </summary>
    'UI64',
    /// <summary>
    /// Floating-point Decimal, 32 bits
    /// </summary>
    'F32',
    /// <summary>
    /// Floating-point Decimal, 64 bits
    /// </summary>
    'F64',
    /// <summary>
    /// Fixed-point Decimal, length and precision handled seperately
    /// </summary>
    'N',
    /// <summary>
    /// Date
    /// </summary>
    'D',
    /// <summary>
    /// Time
    /// </summary>
    'T',
    /// <summary>
    /// Date and Time
    /// </summary>
    'DT',
    /// <summary>
    /// Timestamp
    /// </summary>
    'TS',
    /// <summary>
    /// Binary String of fixed length, length handled seperately
    /// </summary>
    'B',
    /// <summary>
    /// Binary String of variable length, length handled seperately
    /// </summary>
    'VB',
    /// <summary>
    /// String of fixed length, length handled seperately
    /// </summary>
    'C',
    /// <summary>
    /// String of variable length, length handled seperately
    /// </summary>
    'VC',
    /// <summary>
    /// Localizable string of fixed length, length handled seperately
    /// </summary>
    'LC',
    /// <summary>
    /// Localizable string of variable length, length handled seperately
    /// </summary>
    'LVC'
);

module.exports = DbDataType;
