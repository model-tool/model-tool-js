const UTIL = require('../util');

module.exports = class TableCellContent {

  get column() { return this._column; }
  get value() { return this._value; }

  constructor(column, value) {
    this._column = column;
    this._value = value;
  }

}
