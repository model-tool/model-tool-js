module.exports = class SqlCommandGenerator {

  static fileOptions() { return { encoding: 'utf-8' }; }

  constructor() {
    this.maxColNameLen = 0;
    this.maxDataTypeLen = 0;
  }

  getCreateDatabaseCommands(dbName, tables) {}
  getInsertRowCommands(dbName, tableContents) {}

}
