const FS = require('fs');
const PATH = require('path');
const HANDLEBARS = require('./handlebars-helpers.js')(require('handlebars'));

const UTIL = require('../../util');
const DbDataType = require('../DbDataType.js');
const SqlCommandGenerator = require('./SqlCommandGenerator.js');

module.exports = class MySqlCommandGenerator extends SqlCommandGenerator {

  constructor() {
    super();
    // console.log(FS.readFileSync(
    //     PATH.join(__dirname, './templates/MySqlModel.sql.handlebars'),
    //     SqlCommandGenerator.fileOptions()).toString());
    // console.log('A');
    this.modelTpl = HANDLEBARS.compile(
      FS.readFileSync(
        PATH.join(__dirname, './templates/MySqlModel.sql.handlebars'),
        SqlCommandGenerator.fileOptions()).toString());
    // console.log('B');
    this.dataTpl = HANDLEBARS.compile(
      FS.readFileSync(
        PATH.join(__dirname, './templates/MySqlData.sql.handlebars'),
        SqlCommandGenerator.fileOptions()).toString());
    // console.log('C');
  }

  getCreateDatabaseCommands(writer, dbName, tables) {
    writer.write(this.modelTpl({
      'generator': this,
      'dbName': dbName,
      'tables': tables
    }));
  }

  getInsertRowCommands(writer, model, tableContents) {
    writer.write(this.dataTpl({
      'generator': this,
      'model': model,
      'tableContents': tableContents
    }));
  }

  static getObjectTypeCode(dbObject) {
    if (dbObject instanceof EntityDefinition) {
      return 'U';
    }
    return '';
  }

  static getDataType(column) {
    let dataType = '';
    // console.log(column);
    // console.log(column.dataType);
    switch (column.dataType) {
      case DbDataType.UUID:
        dataType = 'binary(16)';
        break;
      case DbDataType.I8:
        dataType = 'tinyint';
        break;
      case DbDataType.I16:
        dataType = 'smallint';
        break;
      case DbDataType.I32:
        dataType = 'int';
        break;
      case DbDataType.I64:
        dataType = 'bigint';
        break;
      case DbDataType.UI8:
        dataType = 'unsigned tinyint';
        break;
      case DbDataType.UI16:
        dataType = 'unsigned smallint';
        break;
      case DbDataType.UI32:
        dataType = 'unsigned int';
        break;
      case DbDataType.UI64:
        dataType = 'unsigned bigint';
        break;
      case DbDataType.F32:
        dataType = 'float';
        break;
      case DbDataType.F64:
        dataType = 'double';
        break;
      case DbDataType.N:
        dataType = `decimal(${column.length}, ${column.precision})`;
        break;
      case DbDataType.D:
        dataType = 'date';
        break;
      case DbDataType.DT:
        dataType = 'datetime';
        break;
      case DbDataType.T:
        dataType = 'time';
        break;
      case DbDataType.B:
        dataType = `binary(${column.length})`;
        break;
      case DbDataType.VB:
        dataType = `varbinary(${column.length})`;
        break;
      case DbDataType.C:
      case DbDataType.LC:
        dataType = `char(${column.length})`;
        break;
      case DbDataType.VC:
      case DbDataType.LVC:
        dataType = `varchar(${column.length})`;
        break;
      default:
        dataType = '';
    }
    return UTIL.rightPad(dataType, this.maxDataTypeLen);
  }

  static getData(cell) {
    if (!UTIL.isNull(cell) &&
        !UTIL.isNull(cell.value) &&
        !UTIL.isNull(cell.column))
    {
      let value = cell.value;
      switch (cell.column.dataType) {
        case DbDataType.UUID:
        case DbDataType.B:
        case DbDataType.VB:
          // console.log(value);
          value = `unhex('${value}')`;
          break;
        case DbDataType.F32:
        case DbDataType.F64:
        case DbDataType.N:
          if (UTIL.isNullOrWhiteSpace(value)) {
            value = (cell.column.isOptional ? 'null' : cell.column.defaultValue);
          }
          else {
            value = value.toLocaleString(undefined, { maximumFractionDigits: cell.column.precision }).replace(',', '');
          }
          break;
        case DbDataType.D:
        case DbDataType.DT:
        case DbDataType.T:
        case DbDataType.C:
        case DbDataType.VC:
        case DbDataType.LC:
        case DbDataType.LVC:
          value = value.toString();
          value = (value.length == 0 && cell.column.isOptional ? 'null' : `'${value}'`);
          break;
        default:
      }
      return value;
    }
    return 'null';
  }

  static escape(str) {
    // console.log(str);
    return str.replace(/'/g, "''");
  }

  toString() { return 'MySqlCommandGenerator'; }

}
