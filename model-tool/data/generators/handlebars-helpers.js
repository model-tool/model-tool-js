module.exports = function registerHelper(HANDLEBARS) {
  HANDLEBARS.registerHelper('backslash', function(options) { return '\\'; });
  HANDLEBARS.registerHelper('not', function(expr, options) { return !expr; });
  HANDLEBARS.registerHelper('and', function(left, right, options) { return left && right; });
  HANDLEBARS.registerHelper('or', function(left, right, options) { return left || right; });
  HANDLEBARS.registerHelper('eq', function(left, right, options) { return left == right; });
  HANDLEBARS.registerHelper('neq', function(left, right, options) { return left != right; });
  HANDLEBARS.registerHelper('lte', function(left, right, options) { return left <= right; });
  HANDLEBARS.registerHelper('lt', function(left, right, options) { return left < right; });
  HANDLEBARS.registerHelper('gte', function(left, right, options) { return left >= right; });
  HANDLEBARS.registerHelper('gt', function(left, right, options) { return left > right; });
  HANDLEBARS.registerHelper('invert', function(iter, options) {
    let ret = [...iter];
    // ret.push(ret[0])
    // console.log(Math.floor(ret.length / 2));
    // console.log(ret.length);
    for (let i = Math.floor(ret.length / 2) - 1; i >= 0; i--) {
      let j = ret.length - i - 1;
      // console.log('i = ' + i.toString() + '; j = ' + j.toString());
      let tmp = ret[i];
      ret[i] = ret[j];
      ret[j] = tmp;
    }
    // console.log(ret.length);
    // console.log(ret);
    return ret;
  });
  HANDLEBARS.registerHelper('call', function(obj, methodName, arg1, arg2, options) {
    // console.log('// ----------');
    // console.log('obj: ' + JSON.stringify(obj));
    // if (methodName == 'getDataType') {
    // if (methodName == 'replace') {
    // if (methodName == 'escape') {
    //   console.log(`// obj: ${obj}, methodName: ${methodName}, arg1: ${arg1}, arg2: ${arg2}`);
    // }
    if (obj &&
      obj[methodName] &&
      typeof obj[methodName] == 'function')
    {
      // It's an instance method.
      let value = obj[methodName](arg1, arg2);
      // console.log(value);
      return value;
      // return obj[methodName]();
    }
    else if (obj &&
      obj['constructor'] &&
      obj.constructor[methodName] &&
      typeof obj.constructor[methodName] == 'function')
    {
      // It's a static method.
      let value = obj.constructor[methodName](arg1, arg2);
      // console.log(value);
      return value;
      // return obj.constructor[methodName]();
    }
    // console.log('<empty>');
    return '';
  });
  return HANDLEBARS;
}
