const MySqlCommandGenerator = require('./MySqlCommandGenerator.js');
const MsSqlCommandGenerator = require('./MsSqlCommandGenerator.js');
const PhpMappingGenerator = require('./PhpMappingGenerator.js');

function getSqlCommandGenerator(dbSystem, writer) {
  let commandGenerator;
  switch (dbSystem) {
    case 'MySql':
    case 'mysql':
    case 'MYSQL':
      commandGenerator = new MySqlCommandGenerator(writer);
      break;
    case 'MsSql':
    case 'mssql':
    case 'MSSQL':
      commandGenerator = new MsSqlCommandGenerator(writer);
      break;
    default:
      commandGenerator = null;
      break;
  }
  return commandGenerator;
}

function getObjectMappingGenerator(codeLanguage) {
  let commandGenerator;
  switch (codeLanguage) {
    case 'PHP':
    case 'Php':
    case 'php':
      commandGenerator = new PhpMappingGenerator();
      break;
    default:
      commandGenerator = null;
      break;
  }
  return commandGenerator;
}

module.exports = {
  forSqlCommands: getSqlCommandGenerator,
  forObjectMappings: getObjectMappingGenerator,
  MySqlCommandGenerator: MySqlCommandGenerator,
  MsSqlCommandGenerator: MsSqlCommandGenerator,
  PhpMappingGenerator: PhpMappingGenerator
}
