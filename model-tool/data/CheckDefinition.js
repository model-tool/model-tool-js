const ConstraintDefinition = require('./ConstraintDefinition.js');

module.exports = class CheckDefinition extends ConstraintDefinition {

  get checkCode() { return this._checkCode; }

  constructor(tableName, columnName, checkCode) {
    super(tableName + columnName + "Ck");
    this._checkCode = checkCode.replace("\r\n", " ").replace("\r", " ").replace("\n", " ");
  }

  toString() { return `Check "${this.constraintName}"`; }

}
