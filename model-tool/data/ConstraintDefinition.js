module.exports = class ConstraintDefinition {

  get constraintName() { return this._constraintName; }

  constructor(constraintName) { this._constraintName = constraintName; }

  toString() { return `Constraint "${this.constraintName}"`; }

}
