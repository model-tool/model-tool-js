module.exports = class TableContent {

  get tableDefinition() { return this._tableDefinition; }
  get columnDefinitions() { return this._columns; }
  get rows() { return this._rows; }

  constructor(tableDefinition, columns = [], rows = []) {
    this._tableDefinition = tableDefinition;
    this._columns = columns;
    this._rows = rows;
  }

}
