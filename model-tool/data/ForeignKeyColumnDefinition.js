module.exports = class ForeignKeyColumnDefinition {

  get columnName() { return this._columnName; }
  get referencedColumnName() { return this._referencedColumnName; }

  constructor(columnName, referencedColumnName) {
    this._columnName = columnName;
    this._referencedColumnName = referencedColumnName;
  }

}
