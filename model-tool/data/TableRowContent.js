module.exports = class TableRowContent {

  get tableDefinition() { return this._tableDefinition; }
  get columnDefinitions() { return this._columns; }
  get cells() { return this._cells; }

  constructor(tableDefinition, columns, cells) {
    this._tableDefinition = tableDefinition;
    this._columns = columns;
    this._cells = cells;
  }

}
