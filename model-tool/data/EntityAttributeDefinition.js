const UTIL = require('../util');
const DbDataType = require('./DbDataType.js');

/**
 * Represents the definition of an entity attribute.
 */
module.exports = class EntityAttributeDefinition {

  get columnId() { return this._columnId; }
  get columnName() { return this._columnName; }
  get columnNamePascalCase() {
    return UTIL.capitalize(this._columnName);
  }
  get columnNameCamelCase() { return UTIL.uncapitalize(this._columnName); }
  get description() { return this._description; }
  get dataType() { return this._dataType; }
  get length() { return this._length; }
  get precision() { return this._precision; }
  get isPrivate() { return this._private; }
  get isOptional() { return this._optional; }

  get isLocalized() {
    return this._dataType == DbDataType.LC || this._dataType == DbDataType.LVC;
  }
  get hasDefaultValue() {
    return !UTIL.isNullOrWhiteSpace(this._defaultValue);
  }

  get isPrimaryKey() { return this._primaryKey; }

  get isForeignKey() {
    return !UTIL.isNullOrWhiteSpace(this._foreignKeyTable) ||
           !UTIL.isNullOrWhiteSpace(this._foreignKeyColumn);
  }

  get defaultValue() { return this._defaultValue; }
  get foreignKeyName() { return this._foreignKeyName; }
  get foreignKeyTable() { return this._foreignKeyTable; }
  get foreignKeyColumn() { return this._foreignKeyColumn; }
  get uniqueKey() { return this._uniqueKey; }
  get checks() { return this._checks; }
  get localizedDescriptions() { return this._localizedDescriptions; }

  constructor(columnId, columnName, description, dataType, length, precision,
    _private, optional, primaryKey, defaultValue,
    foreignKeyName, foreignKeyTable, foreignKeyColumn,
    checks, uniqueKey, localizedDescriptions = new Map())
  {
    this._columnId = columnId;
    this._columnName = columnName;
    this._description = description;
    this._dataType = dataType;
    this._length = UTIL.coalesce(length, 0);
    this._precision = UTIL.coalesce(precision, 0);
    this._private = UTIL.coalesce(_private, false);
    this._optional = UTIL.coalesce(optional, false);
    this._primaryKey = UTIL.coalesce(primaryKey, false);
    this._defaultValue = UTIL.coalesce(defaultValue, null);
    this._foreignKeyName = UTIL.coalesce(foreignKeyName, null);
    this._foreignKeyTable = UTIL.coalesce(foreignKeyTable, null);
    this._foreignKeyColumn = UTIL.coalesce(foreignKeyColumn, null);
    this._uniqueKey = UTIL.coalesce(uniqueKey, null);
    this._checks = UTIL.coalesce(checks, null);
    this._localizedDescriptions = localizedDescriptions;
  }

  hasUniqueKey() { return !UTIL.isNullOrWhiteSpace(this._uniqueKey); }
  isId() { return this._dataType == DbDataType.UUID; }
  valueOf() { return this._columnName; }
  toString() { return `Table Column "${this.columnName}"`; }

}
