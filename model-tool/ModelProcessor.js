const FS = require('fs');
const PATH = require('path');
const XLSX = require('xlsx');
const MemoryStream = require('memorystream');

const UTIL = require('./util');
const FileProcessor = require('./FileProcessor.js');
const ExcelTableRowAccess = require('./ExcelTableRowAccess.js');
const DbDataType = require('./data/DbDataType.js');
const DbModel = require('./data/DbModel.js');
const EntityDefinition = require('./data/EntityDefinition.js');
const EntityAttributeDefinition = require('./data/EntityAttributeDefinition.js');
const GENERATORS = require('./data/generators');

module.exports = class ModelProcessor extends FileProcessor {

  get workingDirPath() { return this._workingDirPath; }
  get baseModelDirPath() { return this._baseModelDirPath; }
  get modelDirPath() { return this._modelDirPath; }
  get sourceDirPath() { return this._sourceDirPath; }
  get createTablesFileName() { return this._createTablesFileName; }
  get model() { return this._model; }
  get codeLanguage() { return this._model.codeLanguage; }
  get dbSystemCode() { return this._model.dbSystemCode; }
  get modelName() { return this._model.modelName; }
  get modelNamespace() { return this._model.modelNamespace; }

  constructor(workingDirPath, baseModelDirPath, modelDirPath, sourceDirPath, baseModelFileName, modelFileName, createTablesFileName) {
    super();
    this._workingDirPath = PATH.normalize(workingDirPath);
    this._baseModelDirPath = baseModelDirPath;
    this._modelDirPath = modelDirPath;
    this._sourceDirPath = sourceDirPath;
    this._baseModelFilePath = PATH.join(baseModelDirPath, baseModelFileName);
    this._modelFilePath = PATH.join(modelDirPath, modelFileName);
    this._createTablesFileName = createTablesFileName;
    this._model = null;
  }

  process() {
    this._sourceDirPath = PATH.join(this._workingDirPath, this._sourceDirPath);
    this._model = new DbModel();
    this.processExcelFile(this._baseModelFilePath);
    this.processExcelFile(this._modelFilePath);
    this.createSqlFiles(this._sourceDirPath, this._createTablesFileName);
    this.createCodeFiles(this._sourceDirPath);
  }

  /**
   * Process the excel file with the given path.
   */
  processExcelFile(excelFilePath) {
    let workbook = XLSX.readFile(excelFilePath);
    let modelSheet = workbook.Sheets['Model'];
    this._model.codeLanguage = 'PHP';
    this._model.dbSystemCode = ExcelTableRowAccess.getCell(modelSheet, 0, 1);
    this._model.modelName = ExcelTableRowAccess.getCell(modelSheet, 1, 1);
    this._model.modelNamespace = ExcelTableRowAccess.getCell(modelSheet, 2, 1);
    this.getDefinitions(workbook, modelSheet);
    // console.log(workbook);
    // console.log(modelSheet);
    // console.log(this);
  }

  /**
   * Get all table definitions available in the given excel workbook.
   */
  getDefinitions(workbook, modelSheet) {
    let excelTable = new ExcelTableRowAccess(modelSheet, 4);

    for (let excelRow of excelTable) {
      let c = 0;
      let tableName = excelRow.getString(c++);
      let localizedSingularDescriptions = new Map();
      let localizedPluralDescriptions = new Map();
      this.getTable(tableName, // tableName
        excelRow.getString(c++), // className
        excelRow.getString(c++), // classNamespace
        excelRow.getString(c++), // baseTableName
        excelRow.getString(c++), // baseClassName
        excelRow.getString(c++), // baseClassNamespace
        excelRow.getStringFlattened(c++), // singularDescription
        excelRow.getStringFlattened(c++), // pluralDescription
        localizedSingularDescriptions, // localizedSingularDescriptions
        localizedPluralDescriptions, // localizedPluralDescriptions
        workbook.Sheets[tableName]); // sheet
      for (let languageCode of excelTable.usedLanguageCodes) {
        // It is expected that two values of the same language
        // are listed in the right order!
        localizedSingularDescriptions.set(languageCode, excelRow.getStringFlattened(c++));
        localizedPluralDescriptions.set(languageCode, excelRow.getStringFlattened(c++));
      }
    }
  }

  /**
   * Get the information of an expected table.
   */
  getTable(tableName, className, classNamespace,
    baseTableName, baseClassName, baseClassNamespace,
    singularDescription, pluralDescription,
    localizedSingularDescriptions, localizedPluralDescriptions,
    sheet)
  {
    let baseTable = null;
    if (!UTIL.isNullOrWhiteSpace(baseTableName) &&
        !UTIL.isNullOrWhiteSpace(baseClassNamespace))
    {
      for (let table of this._model.getEntitiesByClassNamespace(baseClassNamespace)) {
        if (table.tableName == baseTableName) {
          baseTable = table;
          break;
        }
      }
    }
    let entity = new EntityDefinition(tableName,
      className, classNamespace,
      baseTable, baseTableName, baseClassName, baseClassNamespace,
      singularDescription, pluralDescription,
      localizedSingularDescriptions, localizedPluralDescriptions);
    this._model.addEntity(entity);

    let excelTable = new ExcelTableRowAccess(sheet);

    for (let excelRow of excelTable) {
      let c = 0;
      let columnName = excelRow.getString(c++);
      let columnDescription = excelRow.getStringFlattened(c++);
      let localizedDescriptions = new Map();
      let dataType = DbDataType[excelRow.getString(c++)];
      entity.addAttribute(new EntityAttributeDefinition(
        excelRow.rowIndex, // columnId
        columnName, columnDescription, dataType, // dataType
        excelRow.getInteger(c++), // length
        excelRow.getInteger(c++), // precision
        excelRow.getBoolean(c++), // private
        excelRow.getBoolean(c++), // optional
        excelRow.getBoolean(c++), // primaryKey
        excelRow.getString(c++), // defaultValue
        excelRow.getString(c++), // foreignKeyName
        excelRow.getString(c++), // foreignKeyTable
        excelRow.getString(c++), // foreignKeyColumn
        excelRow.getString(c++), // checks
        excelRow.getString(c++),  // uniqueKey
        localizedDescriptions));
      c++; // Excel table column "Indexes" currently unused.
      for (let languageCode of excelTable.usedLanguageCodes) {
        localizedDescriptions.set(languageCode, excelRow.getStringFlattened(c++));
      }
    }
  }

  /**
   * Create the sql files representing the model.
   */
  createSqlFiles(sourceDirPath, createTablesFileName) {
    let createTablesSqlFilePath = PATH.join(__dirname, sourceDirPath, 'sql', createTablesFileName);
    // console.log(__dirname);
    // console.log(sourceDirPath);
    // console.log(createTablesFileName);
    if (!UTIL.isNullOrWhiteSpace(createTablesSqlFilePath)) {
      createTablesSqlFilePath = PATH.normalize(createTablesSqlFilePath);
      // console.log(createTablesSqlFilePath);
      // let writer = IoHelper.getNewTextFileWriter(createTablesSqlFilePath);
      let writer = UTIL.createWriteStream(createTablesSqlFilePath, FileProcessor.fileOptions());
      // let writer = new MemoryStream();
      let sqlGenerator = GENERATORS.forSqlCommands(this._model.dbSystemCode);
      // console.log(this);
      sqlGenerator.getCreateDatabaseCommands(writer, this._model.modelName, this._model.tables);
      writer.end();
    }
  }

  /**
   * Create the code files representing the model.
   */
  createCodeFiles(sourceDirPath) {
    let mappingGenerator = GENERATORS.forObjectMappings(this._model.codeLanguage);
    let mappingPath = PATH.join(__dirname, sourceDirPath, mappingGenerator.fileExtension, this._model.modelNamespace);
    let filepath = PATH.join(mappingPath, `${this._model.modelName}Context.${mappingGenerator.fileExtension}`);
    let writer = UTIL.createWriteStream(filepath, FileProcessor.fileOptions());
    mappingGenerator.writeContextClass(writer, this._model);
    writer.end();

    for (let entity of this._model.classes) {
      let mappingPath = PATH.join(__dirname, sourceDirPath, mappingGenerator.fileExtension, entity.classNamespace);
      filepath = PATH.join(mappingPath, `${entity.className}Table.${mappingGenerator.fileExtension}`);
      writer = UTIL.createWriteStream(filepath, FileProcessor.fileOptions());
      mappingGenerator.writeTableClass(writer, this._model.modelName, entity);
      writer.end();
      filepath = PATH.join(mappingPath, `${entity.className}Row.${mappingGenerator.fileExtension}`);
      writer = UTIL.createWriteStream(filepath, FileProcessor.fileOptions());
      mappingGenerator.writeRowClass(writer, entity);
      writer.end();
    }
  }

}
